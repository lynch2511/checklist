# webbd-lab3

Projet de développement web en python (Flask)

CONSIGNES D'UTILISATION

0. Installer les dépendances données ci-dessous
1. Ouvrir un terminal dans le répertoire de votre choix 
2. Saisir `git clone https://gitlab.com/lynch2511/checklist.git`
3. Saisir `export FLASK_APP=run.py`
4. Saisir `flask run`
5. Ouvrez un navigateur internet, puis saisir dans la barre d'adresse, l'adresse indiquée dans le terminal, par défaut saisir http://127.0.0.1:5000/
6. La page d'accueil s'affiche.

Dépendances : 

click==7.1.2
Flask==1.1.2
flask-blueprint==1.3.0
Flask-SQLAlchemy==2.5.1
greenlet==1.1.0
itsdangerous==1.1.0
Jinja2==2.11.3
MarkupSafe==1.1.1
psycopg2-binary==2.8.6
SQLAlchemy==1.4.15
Werkzeug==1.0.1
flask-wtf
