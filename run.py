#! /usr/bin/env python
from app import app

if __name__ == "__main__":
    app.secret_key = '#d#JCqTTW\nilK\\7m\x0bp#\tj~#H'
    app.config['SESSION_TYPE'] = 'filesystem'

    app.debug = True
    app.run()