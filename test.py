from flask import Flask
from flask import g

import sqlite3

DATABASE = 'checklist.db'

app = Flask(__name__)

def get_db():
	db = getattr(g, '_database', None)
	if db is None:
		db = g._database = sqlite3.connect(DATABASE)
	return db

@app.teardown_appcontext
def close_connection(exception):
	db = getattr(g, '_database',None)
	if db is not None:
		db.close()

@app.route('/')
def index():
	return'Le site est lancé !'

@app.route('/tasks')
def all_lists():
	c = get_db().cursor()
	c.execute("select * from liste")

	content = '<b> Listes</b>'
	content+='<ul>'
	for tpl in c.fetchall():
		content += f'<li>{tpl[1]}</li>'
	content+= '</ul>'
	return content

@app.route('/lists/<int:list_id>')
def one_list(list_id):
	return 'Identifiant de la liste = {list_id}'