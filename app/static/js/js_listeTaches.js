
$(document).ready(function() {

    var tableTache;
    var tableTacheTerminees;

    // Peuplement des datatables
    selectTachesEnCours();
    selectTachesTerminees();

    // Terminer une tâche
    $("#dataTableEnCours").on("mousedown", "td .far.fa-check-square", function(e) {
         
        var id = parseInt($(this).closest("tr").index())
        var idbdd = $("#dataTableEnCours")[0].tBodies[0].rows[id].cells[5].innerText
        // Ouverture de la liste
        terminerTask(idbdd);    
        reload();
    });

    // Afficher les détails de la tâche
    $("#dataTableEnCours").on("mousedown", "td .fas.fa-search", function(e) {
        var id = parseInt($(this).closest("tr").index())
        var priorite = $("#dataTableEnCours")[0].tBodies[0].rows[id].cells[0].innerText
        var prioriteNom = $("#dataTableEnCours")[0].tBodies[0].rows[id].cells[1].innerText
        var nom = $("#dataTableEnCours")[0].tBodies[0].rows[id].cells[2].innerText
        var description = $("#dataTableEnCours")[0].tBodies[0].rows[id].cells[3].innerText
        var echeance = $("#dataTableEnCours")[0].tBodies[0].rows[id].cells[4].innerText
        var tacheId = $("#dataTableEnCours")[0].tBodies[0].rows[id].cells[5].innerText
        var text = '';
        text ='' 
        text += '<i class="fas fa-tasks fa-4x"></i></br>'    
        text += '<div class="m-3">Tâche (ID) : '+tacheId+'</div>'
        text += '<div class="m-3">Priorité (ID) : '+priorite+'</div>'
        text += '<div class="m-3">Priorité (Nom) : '+prioriteNom+'</div>'
        text += '<div class="m-3">Tâche (Echéance) : '+echeance+'</div>'
        text += '<textarea class="m-3" placeholder=\"'+description+'\"></textarea>'
        Swal.fire({
            html: text,
            title: 'Détails de la tâche \n"'+nom+'"\n',
        })
    });

    // Suppression d'une tâche
    $("#dataTableEnCours").on("mousedown", "td .fas.fa-trash-alt", function(e) {
        var id = parseInt($(this).closest("tr").index())+1
        var idbdd = $("#dataTableEnCours")[0].rows[id].cells[5].innerText
        // Envoi en bdd
        deleteTaskCall(idbdd);
        // Suppression de l'interface
        tableTache.row($(this).closest("tr")).remove().draw();   
    });

    // Ouverture des champs pour modification d'une tâche
    $("#dataTableEnCours").on('mousedown.edit', "i.fa.fa-pencil-square", function(e) {
        $(this).removeClass().addClass("fa fa-check");
        var $row = $(this).closest("tr").off("mousedown");
        var $tds = $row.find("td");
        var idRow = parseInt($(this).closest("tr").index())
        
        $.each($tds, function(i, el) {
            var txt = $(this).text();
            var td = $(this);
            if(i == 1){
                // Mettre un champ de type select pour sélectionner la priorité
                // Récupérer les priorités 
                $.ajax({
                    url: '/getPriorites',
                    dataType: 'json',
                    success:function(response){
                        var len = response.data.length;
                        td.html("").append("<select id=\"selectpriorite"+idRow+"\" name=\"selectbasic\" class=\"form-control\"></select>");
                        for( var i = 0; i<len; i++){
                            var id = response.data[i]['idPriorite'];
                            var name = response.data[i]['nomPriorite'];     
                            // Ajout des priorités dans le select    
                            $("#selectpriorite"+idRow).append("<option value='"+id+"'>"+name+"</option>");
                        }
                        $("#selectpriorite"+idRow)[0].value = parseInt($("#dataTableEnCours")[0].tBodies[0].rows[idRow].cells[0].innerText)
                    }
                });
            }else if(i==4){
                // Date échéance
                $(this).html("").append("<input class=\"col-12\" type='date' value=\""+txt+"\">");
            }else if(i==2 || i==3){
                $(this).html("").append("<input class=\"col-12\" type='text' value=\""+txt+"\">");
            }       
        });
    });

    $("#dataTableEnCours").on('mousedown', "input", function(e) {
    //    e.stopPropagation();
    });

    // Coloration auto de la ligne si la ligne est redessinée
    // $('#dataTableTerminees').on('fnRowCallback', function(e){
    //     var $row = $(this).closest("tr");
    //     var rowIndex = $(this).closest("tr").index();
    //     var color  = $('#dataTableEnCours')[0].tBodies[0].rows[rowIndex].cells[5].innerText;
    //     if(color != ''){
    //         $('td', $row).css('background-color', color);
    //         $('td', nRow).css('color', 'white');
    //     }                
    // });

    // Sauvegarde d'une tâche en bdd
    $("#dataTableEnCours").on('mousedown.save', "i.fa.fa-check", function(e) {
        $(this).removeClass().addClass("fa fa-pencil-square");
        var $row = $(this).closest("tr");
        var idRow = parseInt($(this).closest("tr").index());
        var $tds = $row.find("td");   
        var idPrioriteNew = $("#selectpriorite"+idRow)[0].value
        
        $.each($tds, function(i, el) {
            if(i ==4 || i == 3 || i == 2 || i == 1){
                var txt = $(this).find("input").val();
                $(this).html(txt);
            }   
        });
        // Mise à jour de la colonne priorite (ID) selon la valeur sélectionnée par l'utilisateur dans Priorité (nom)
        $("#dataTableEnCours")[0].rows[(idRow)+1].cells[0].innerText = idPrioriteNew;
        // Enregistrement en bdd    
        saveChanges($(this).closest("tr").index());
    });
    
    // Drag drop
    $("#dataTableEnCours").on('mousedown', "#selectbasic", function(e) {
        //supprime le reste de la propagation, en capture et bubbling
    e.stopPropagation();
    });

    // Ajout du bouton d'ajout d'une ligne
    $('#dataTableEnCours').css('border-bottom', 'none');
    $('<div class="addRow m-3"><button type="button" id="addRowTache" class="btn btn-success float-left fas fa-plus-square"> Ajouter une tâche</button></div>').insertAfter('#dataTableEnCours');

    // Ajout d'une tâche
    $('#addRowTache').click(function() {
        var nbRow = $('#dataTableEnCours')[0].tBodies[0].rows.length
        var prioriteDefaut = 0;
        if($('#dataTableEnCours')[0].tBodies[0].rows[0].childNodes.length == 1){
            prioriteDefaut = 1;       
            nbRow = 0
        }
        else {
            // On reprend la priorite de la dernière ligne existante
            prioriteDefaut = parseInt($('#dataTableEnCours')[0].tBodies[0].rows[nbRow-1].cells[0].innerText);      
        }
        // Ajout dans le tableau
        tableTache.row.add({"Priorite (Ordre)": prioriteDefaut,"Priorite (Nom)" : "<select id=\"selectpriorite"+nbRow+"\" name=\"selectbasic\" class=\"form-control\"></select>", "Nom": "Nouvelle tâche...", "Description": "Saisir une description...", "Echeance": "9999-99-9", "Couleur": "white", "Tache (ID)": '', "Actions": "<i id=btEditSave"+nbRow+" class=\"fa fa-pencil-square\" title=\"Editer\" aria-hidden=\"true\"></i> <i class=\"fas fa-trash-alt\" title=\"Supprimer\" aria-hidden=\"true\"></i><i id=\"terminer\" class=\"far fa-check-square ml-2\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"Terminer\" aria-hidden=\"false\"></i>"}).draw();
        
        // Récupérer les priorités 
        $.ajax({
            url: '/getPriorites',
            dataType: 'json',
            success:function(response){
                var len = response.data.length;
                $("#selectpriorite"+nbRow).empty();
                for( var i = 0; i<len; i++){
                    var id = response.data[i]['idPriorite'];
                    var name = response.data[i]['nomPriorite'];     
                    // Ajout des priorités dans le select    
                    $("#selectpriorite"+nbRow).append("<option value='"+id+"'>"+name+"</option>");
                }
            }
        });
        
        // Rendre modifiables les champs
        $tds = $('#dataTableEnCours')[0].tBodies[0].rows[nbRow].childNodes;
            $.each($tds, function(i, el) {
                if(i != 0 && i!=1 && i!= 6 && i!=5){
                    var txt = $(this).text();
                    $(this).html("").append("<input type='text' value=\""+txt+"\">");
                } 
                if(i==6){
                    $('#btEditSave'+nbRow).removeClass( "fa-pencil-square" ).addClass( "fa fa-check" );
                }  
            });  
        });

        // Sauvegarde les modifications de la saisie sur la ligne d'une tâche
        function saveChanges(indexRow){
            var idTache = $('#dataTableEnCours')[0].tBodies[0].rows[indexRow].cells[5].innerText;
            if(idTache != ''){
                //UPDATE
                // Envoi en bdd
                var priorite = $('#dataTableEnCours')[0].tBodies[0].rows[indexRow].cells[0].innerText;
                var e = document.getElementById('selectpriorite'+indexRow+'');
                var nom = $('#dataTableEnCours')[0].tBodies[0].rows[indexRow].cells[2].innerText;
                var description = $('#dataTableEnCours')[0].tBodies[0].rows[indexRow].cells[3].innerText;
                var echeance = $('#dataTableEnCours')[0].tBodies[0].rows[indexRow].cells[4].innerText;
                var idTache = $('#dataTableEnCours')[0].tBodies[0].rows[indexRow].cells[5].innerText;
                updateTaskCall(priorite,nom,description,echeance,idTache)
            }
            else {
                // INSERT
                // Envoi bdd
                var priorite = $('#dataTableEnCours')[0].tBodies[0].rows[indexRow].cells[0].innerText;
                var e = document.getElementById('selectpriorite'+indexRow+'');
                var prioriteNom = e.options[e.selectedIndex].text;
                var nom = $('#dataTableEnCours')[0].tBodies[0].rows[indexRow].cells[2].innerText;          
                var description = $('#dataTableEnCours')[0].tBodies[0].rows[indexRow].cells[3].innerText;
                var echeance = $('#dataTableEnCours')[0].tBodies[0].rows[indexRow].cells[4].innerText;
                insertTaskCall(priorite,nom,description,echeance);
            }
            reload();
        }

    // CRUD Listes appels ---
    function selectTachesEnCours(){
            // Chargement des données dans les datatable
            try {
                tableTache = $('#dataTableEnCours').DataTable({            
                    ajax: '/getTasksEnCours/'+document.getElementById('list_id').innerText,
                    fnRowCallback: function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                        $('td', nRow).css('background-color', aData.Couleur);
                        if(aData.Couleur != 'white'){
                            $('td', nRow).css('color', 'white');
                        }
                        else {
                            $('td', nRow).css('color', 'black');
                        }
                    },
                    columns: [{
                    data: 'Priorite (Ordre)'
                    }, {
                    data: 'Priorite (Nom)'
                    }, {
                    data: 'Nom'
                    }, {
                    data: 'Description'
                    }, {
                    data: 'Echeance'
                    }, {
                    data: 'Couleur',
                    visible: false
                    }, {
                    data: 'Tache (ID)'
                    }, {
                    data: 'Actions'
                    }]       
                });
            } catch (error) {
       
            }  
    }

    function selectTachesTerminees(){
        try {
            tableTacheTerminees = $('#dataTableTerminees').DataTable({
                ajax: '/getTasksTerminees/'+document.getElementById('list_id').innerText,
                fnRowCallback: function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                    $('td', nRow).css('background-color', aData.Couleur);
                    $('td', nRow).css('color', 'white');
                },
                columns: [{
                data: 'Priorite (Ordre)'
                }, {
                data: 'Priorite (Nom)'
                }, {
                data: 'Nom'
                }, {
                data: 'Description'
                }, {
                data: 'Echeance'
                }, {
                data: 'Couleur',
                visible: false
                }, {
                data: 'Tache (ID)'
                }]
            });
        } catch (error) {
            
        }
    }

    // Recharge les datatables
    function reload(){
        setTimeout(function() {
            tableTache.ajax.reload();
            tableTacheTerminees.ajax.reload();
          }, 1000);   
    }

    // Ajoute une nouvelle liste en base de données
    function insertTaskCall(priorite,nom,description,echeance){
        $.ajax({
            type : 'POST',
            url : "/insertTask",
            contentType: 'application/json;charset=UTF-8',
            data : JSON.stringify({'nom':nom, 'priorite':priorite, 'description':description,'echeance':echeance, 'idList':document.getElementById('list_id').innerText})
        });
    }

    //Modifie une tâche en bdd
    function updateTaskCall(priorite, nom,description,echeance,idTache){
        $.ajax({
            type : 'POST',
            url : "/updateTask",
            contentType: 'application/json;charset=UTF-8',
            data : JSON.stringify({'idTache':idTache, 'nom':nom, 'priorite':priorite, 'description':description,'echeance':echeance, 'idList':document.getElementById('list_id').innerText})

        });
    }

    // Passe une tâche à l'état terminé
    function terminerTask(idTache){
        $.ajax({
            type : 'POST',
            url : "/endTask",
            contentType: 'application/json;charset=UTF-8',
            data : JSON.stringify({'idTache':idTache})

        });
    }

    // Efface une tâche en bdd
    function deleteTaskCall(idTache){
        $.ajax({
            type : 'POST',
            url : "/deleteTask",
            contentType: 'application/json;charset=UTF-8',
            data : JSON.stringify({'id':idTache})
        });
    }

});
