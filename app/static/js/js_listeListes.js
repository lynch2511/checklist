$(document).ready(function() {

    var tableListes;

    selectAllLists();

    // Suppression d'une ligne
    $("#dataTableListes").on("mousedown", "td .fas.fa-trash-alt", function(e) {
         
        var id = parseInt($(this).closest("tr").index())+1
        var idbdd = $("#dataTableListes")[0].rows[id].cells[0].innerText
        // Envoi en bdd
        deleteListCall($("#dataTableListes")[0].rows[id].cells[0].innerText);
        // Suppression de l'interface
        //tableListes.row($(this).closest("tr")).remove().draw(); 
        reload();      
        
    })

    // Ouvrir une liste
    $("#dataTableListes").on("mousedown", "td .far.fa-folder-open", function(e) {
         
        var id = parseInt($(this).closest("tr").index())
        var idbdd = $("#dataTableListes")[0].tBodies[0].rows[id].cells[0].innerText
        // Ouverture de la liste
        showList(idbdd);    
        
    })

    // Ouverture des champs de la ligne pour édition
    $("#dataTableListes").on('mousedown.edit', "i.fa.fa-pencil-square", function(e) {

      $(this).removeClass().addClass("fa fa-check");
      var $row = $(this).closest("tr").off("mousedown");
      var $tds = $row.find("td").not(':first').not(':last');

      $.each($tds, function(i, el) {
        var txt = $(this).text();
        $(this).html("").append("<input type='text' value=\""+txt+"\">");
      });

    });

    // Drag drop
    $("#dataTableListes").on('mousedown', "input", function(e) {
      e.stopPropagation();
    });

    // Sauvegarde de la ligne
    $("#dataTableListes").on('mousedown.save', "i.fa.fa-check", function(e) {
      
      $(this).removeClass().addClass("fa fa-pencil-square");
      var $row = $(this).closest("tr");
      var idRow = $(this).closest("tr").index();
      var $tds = $row.find("td").not(':first').not(':last');
      
      $.each($tds, function(i, el) {
        var txt = $(this).find("input").val()
        $(this).html(txt);
      });

      // Envoi en bdd
      if($('#dataTableListes')[0].tBodies[0].rows[idRow].cells[0].innerText == ''){
          // INSERT
        insertListCall($('#dataTableListes')[0].tBodies[0].rows[idRow].cells[1].innerText);
      }
      else {
         // UPDATE
        updateListCall($("#dataTableListes")[0].tBodies[0].rows[idRow].cells[0].innerText,$("#dataTableListes")[0].tBodies[0].rows[idRow].cells[1].innerText)
      }
        reload();
    });
       
     $("#dataTableListes").on('mousedown', "#selectbasic", function(e) {
      e.stopPropagation();
    });

    // Ajout du bouton ajout de ligne
    $('#dataTableListes').css('border-bottom', 'none');
    $('<div class="addRow m-3"><button type="button" id="addRowListe" class="btn btn-success float-left fas fa-plus-square"> Créer une checklist</button></div>').insertAfter('#dataTableListes');

    // Ajout d'une liste
    $('#addRowListe').click(function() {
      var nbRow = $('#dataTableListes')[0].tBodies[0].rows.length;
      if($('#dataTableListes')[0].tBodies[0].rows[0].childNodes.length == 1){     
          nbRow = 0
      }
      tableListes.row.add({"Liste (ID)": '', "Nom": "Nouvelle liste", "Actions": "<i id=btEditSave"+nbRow+" class=\"fa fa-pencil-square\" aria-hidden=\"true\"></i> <i class=\"fas fa-trash-alt\" aria-hidden=\"true\"></i><i id=\"Ouvrir\" class=\"far fa-folder-open ml-2\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"Ouvrir\" aria-hidden=\"false\"></i>"}).draw();
      
      // Rendre modifiables les champs
      $tds = $('#dataTableListes')[0].tBodies[0].rows[nbRow].childNodes;
          $.each($tds, function(i, el) {
              if(i == 1){
                  var txt = $(this).text();
                  $(this).html("").append("<input type='text' value=\""+txt+"\">");
              } 
              if(i==2){
                  $('#btEditSave'+nbRow).removeClass("fa-pencil-square" ).addClass("fa fa-check" );
              }  
          });  
    });

    // CRUD Listes appels ---
    function showList(idList){
        $.ajax({
            type : 'GET',
            success: function(data,textStatus){
                    window.location.href = '/liste/'+idList
            }
          });
    }

    // Récupère toutes les listes de l'utilisateur connecté
    function selectAllLists(){       
      tableListes = $('#dataTableListes').DataTable({
        ajax: '/getListesUser',
        columns: [{
        data: 'Liste (ID)'
        }, {
        data: 'Nom'
        },  {
        data: 'Actions'
        }]
    });
    }
   
    // Ajoute une nouvelle liste en base de données
    function insertListCall(nom){
        $.ajax({
            type : 'POST',
            url : "/insertList",
            contentType: 'application/json;charset=UTF-8',
            data : JSON.stringify({'nomListe':nom})
          });
    }

    //Modifie une liste en bdd
    function updateListCall(id, nom){
        $.ajax({
            type : 'POST',
            url : "/updateList",
            contentType: 'application/json;charset=UTF-8',
            data : JSON.stringify({'id':id, 'nom':nom})
          });
    }

    // Efface une liste en bdd
    function deleteListCall(id){
        $.ajax({
            type : 'POST',
            url : "/deleteList",
            contentType: 'application/json;charset=UTF-8',
            data : JSON.stringify({'id':id})
          });
    }
    // Recharge les datatables
    function reload(){
      setTimeout(function() {
        tableListes.ajax.reload();
      }, 1000);  
      
     
    }

  });