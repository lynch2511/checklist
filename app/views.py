from config import SECRET_KEY
from types import new_class
from typing import OrderedDict
from test import DATABASE
from flask import Flask, render_template, jsonify, request, flash, redirect
from flask_wtf.csrf import CSRFProtect
from flask import g
import sqlite3
import pprint
import json

DATABASE = 'checklist.db'
app = Flask(__name__)
app.secret_key = "#d#JCqTTW\nilK\\7m\x0bp#\tj~#Hsdfoijsdfoih"	

# Test : il n'y a pas d'implémentation de connexion /déconnexion donc on fixe un utilisateur de test
ID_UTILISATEUR = 1

def get_db():
	db = getattr(g, '_database', None)
	if db is None:
		db = g._database = sqlite3.connect(DATABASE)
	return db

@app.teardown_appcontext
def close_connection(exception):
	db = getattr(g, '_database',None)
	if db is not None:
		db.close()

@app.route('/')
def index():
    return render_template('home.html')

@app.route('/mylists')
def getMyLists():
	return render_template('liste_listes.html', id_user = ID_UTILISATEUR)

# Récupère une liste donnée
@app.route('/liste/<list_id>')
def getList(list_id):
	if int(list_id) != None and int(list_id) > -1:
		c = get_db().cursor()
		c.execute("select liste.* FROM liste INNER JOIN liste_has_utilisateur ON liste.liste_id = liste_idliste where liste_id ='"+str(list_id)+"' AND liste_has_utilisateur.utilisateur_idutilisateur = "+str(ID_UTILISATEUR)+";")
		liste = c.fetchone()
		c.connection.close()
		list_id = liste[0]
		nomListe = liste[1]
		return render_template("liste_taches.html", list_id = list_id, nom_liste = nomListe)

# Appel Ajax
@app.route('/getTasksEnCours/<list_id>')
def getTasksEnCours(list_id):
	c = get_db().cursor()
	c.execute("select tache.*, priorite.* from tache inner join priorite on tache.priorite_idpriorite = priorite.idpriorite where etat = false and liste_idliste = "+str(list_id)+";")
	listeEnCoursbdd = c.fetchall()	
	listeEnCours = []
	for tache in listeEnCoursbdd:
		sousListeTache = {}
		sousListeTache["Priorite (Ordre)"] = tache[1]
		sousListeTache["Priorite (Nom)"] = tache[9]
		sousListeTache["Nom"] = tache[4]
		sousListeTache["Description"] = tache[5]
		sousListeTache["Echeance"] = tache[6]
		sousListeTache["Tache (ID)"] = tache[0]
		sousListeTache["Couleur"] = tache[10]
		sousListeTache["Actions"] = "<i class=\"fa fa-pencil-square\" aria-hidden=\"true\"></i> <i class=\"fas fa-trash-alt m-2\" aria-hidden=\"true\"></i><i id=\"terminer\" class=\"far fa-check-square ml-2\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"Terminer\" aria-hidden=\"false\"></i>"
		listeEnCours.append(sousListeTache)
	return jsonify(data = listeEnCours)

# Appel ajax
@app.route('/getTasksTerminees/<list_id>')
def getTasksTerminees(list_id):
	c = get_db().cursor()
	c.execute("select tache.*, priorite.* from tache inner join priorite on tache.priorite_idpriorite = priorite.idpriorite where etat = true and liste_idliste = '"+str(list_id)+"';")
	listeTermineesBdd = c.fetchall()
	listeTerminees = []
	for tache in listeTermineesBdd:
		sousListeTache = {}
		sousListeTache["Priorite (Ordre)"] = tache[1]
		sousListeTache["Priorite (Nom)"] = tache[9]
		sousListeTache["Nom"] = tache[4]
		sousListeTache["Description"] = tache[5]
		sousListeTache["Echeance"] = tache[6]
		sousListeTache["Couleur"] = tache[10]
		sousListeTache["Tache (ID)"] = tache[0]
		listeTerminees.append(sousListeTache)
	return jsonify(data = listeTerminees)

# Appel ajax pour passer les priorités
@app.route('/getPriorites')
def getPriorites():
	c = get_db().cursor()
	c.execute("select * from priorite;")
	listePriorites = c.fetchall()
	listeTerminees = []
	for priorite in listePriorites:
		sousListeTache = {}
		sousListeTache["idPriorite"] = priorite[0]
		sousListeTache["nomPriorite"] = priorite[1]
		sousListeTache["couleur"] = priorite[2]
		listeTerminees.append(sousListeTache)
	return jsonify(data = listeTerminees)


# Récupérer les checklists de l'utilisateur
@app.route('/getListesUser')
def getListesUser():
	c = get_db().cursor()
	c.execute("select liste.* from liste inner join liste_has_utilisateur on liste.liste_id = liste_has_utilisateur.liste_idliste where utilisateur_idutilisateur = "+str(ID_UTILISATEUR)+";")
	listeListesBdd = c.fetchall()
	listes = []
	for tache in listeListesBdd:
		sousListeListe = {}
		sousListeListe["Liste (ID)"] = tache[0]
		sousListeListe["Nom"] = tache[1]
		sousListeListe["Actions"] = "<i class=\"fa fa-pencil-square\" aria-hidden=\"true\"></i> <i class=\"fas fa-trash-alt\" aria-hidden=\"true\"></i><i id=\"Ouvrir\" class=\"far fa-folder-open ml-2\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"Ouvrir\" aria-hidden=\"false\"></i>"
		listes.append(sousListeListe)
	return jsonify(data = listes)

# Suppression d'une Liste
@app.route('/deleteList', methods=['POST'])
def deleteList():
	id = None
	id = request.json['id']
	if id != None:
		try:
			print(id)
			c = get_db().cursor()
			# Supprime en cascade les tâches associées
			c.execute("DELETE FROM tache where liste_idliste ='"+str(id)+"';")
			# Supprime en cascade l'association utilisateur-liste
			c.execute("DELETE FROM liste_has_utilisateur where liste_idliste ='"+str(id)+"';")
			# Supprime la liste 
			c.execute("DELETE FROM liste where liste_id ='"+str(id)+"';")
			c.connection.commit()
			c.connection.close()
			flash("Liste correctement supprimée !")
		except sqlite3.IntegrityError as e:
			flash(e)
	return redirect("/mylists", code=302)
# Update d'une Liste
@app.route('/updateList', methods=['POST'])
def updateList():
	id = None
	nom = None
	id = request.json['id']
	nom = request.json['nom']
	print(id,nom)
	if id != None and nom != None:
		try:
			c = get_db().cursor()		
			c.execute("UPDATE liste SET nom='"+nom+"' where liste_id ='"+str(id)+"';")
			c.connection.commit()
			c.connection.close()
			flash("Liste correctement modifiée !")
		except sqlite3.IntegrityError as e:
			flash(e)
	return redirect("/mylists", code=302)
	
# Insert d'une Liste
@app.route('/insertList', methods=['POST'])
def insertList():
	nom = None
	nom = request.json['nomListe']
	if nom != None:
		c = get_db().cursor()
		try:
			print(nom)
			c.execute('insert into liste(nom) values (?)',(nom,))	
			c.execute('select liste_id from liste order by liste_id desc limit 1')
			# .. l'auto incrément sur la clé primaire ne semble pas fonctionner avec sqlite bien qu'elle soit nommée liste_id...
			result = c.fetchone()
			if result is None:
				liste_id_new = 1
			else:
				liste_id_new = result[0]
			c.execute('insert into liste_has_utilisateur (liste_idliste, utilisateur_idutilisateur) values (?,?)',(liste_id_new,ID_UTILISATEUR))
			c.connection.commit()
			c.connection.close()
			flash("Liste correctement ajoutée !")
		except sqlite3.IntegrityError as e:
			flash(e)
	return redirect("/mylists", code=302)

@app.route('/insertTask', methods=['POST'])
def insertTask():
	priorite = None
	nom = None
	description = None
	echeance = None
	idList = None
	priorite = request.json['priorite']
	nom = request.json['nom']
	description = request.json['description']
	echeance = request.json['echeance']
	idList = request.json['idList']
	if priorite != None and nom != None and description != None and echeance != None and idList != None:
		c = get_db().cursor()
		try:
			print(priorite)
			c.execute('insert into tache (priorite_idpriorite, liste_idliste, utilisateur_idutilisateur, nom, description, dateFin, etat) values (?,?,?,?,?,?,?)',(priorite, idList, ID_UTILISATEUR, nom, description, echeance,0))
			c.connection.commit()
			c.connection.close()
			flash("Tâche correctement ajoutée !")
		except sqlite3.IntegrityError as e:
			flash(e)
	return redirect("/liste/"+idList, code=302)

# Update d'une Tache
@app.route('/updateTask', methods=['POST'])
def updateTask():
	priorite = None
	nom = None
	description = None
	echeance = None
	idTache = None
	idList = None
	priorite = request.json['priorite']
	nom = request.json['nom']
	description = request.json['description']
	echeance = request.json['echeance']
	idTache = request.json['idTache']
	idList = request.json['idList']
	if priorite != None and nom != None and description != None and echeance != None and idTache != None and idList != None:
		try:
			c = get_db().cursor()		
			c.execute("UPDATE tache SET nom='"+nom+"', priorite_idPriorite='"+priorite+"', description='"+description+"', dateFin='"+echeance+"' where tache_id ='"+str(idTache)+"';")
			c.connection.commit()
			c.connection.close()
			flash("Liste correctement modifiée !")
		except sqlite3.IntegrityError as e:
			flash(e)
	return redirect("/liste/"+idList, code=302)	

# Terminer une tâche
@app.route('/endTask', methods=['POST'])
def endTask():
	id = None
	id = request.json['idTache']
	if id != None:
		try:
			c = get_db().cursor()
			c.execute("UPDATE tache SET etat=True where tache_id ='"+str(id)+"';")
			c.connection.commit()
			c.connection.close()
			flash("Tâche correctement mise à jour !")
		except sqlite3.IntegrityError as e:
			flash(e)
	return redirect("/liste/"+id, code=302)	

# Suppression d'une tâche
@app.route('/deleteTask',methods=['POST'])
def deleteTask():
	id = None
	id = request.json['id']
	if id != None:
		try:
			c = get_db().cursor()
			# Supprime en cascade les tâches associées
			c.execute("DELETE FROM tache where tache_id ='"+str(id)+"';")
			c.connection.commit()
			c.connection.close()
			flash("Tâche correctement supprimée !")
		except sqlite3.IntegrityError as e:
			flash(e)
	return redirect("/mylists", code=302)



